/*
 * ARDUINO UNO THERMAL CYCLER
 * For use with the Polymerase Chain Reaction (PCR) process
 * 
 * A Global Ties - Engineering World Health Team Project
 *
 * Team:
 * Alex Smith
 * Nasim Nia
 * Al Jelvani
 * Nathan Quach
*/ 

#include <avr/sleep.h>
#include <math.h>
#define pin_thermistor   0
#define pin_heater_PWR   6
#define pin_heater_DIR   7
#define pin_fan_PWR      11
#define pin_fan_DIR      12

/* Temperature Defaults in Celsius */
double TEMP_CEILING   =  95;
double TEMP_FLOOR     =  60;
double WARNING_LIMIT  = 300;

int MAX_CYCLES        =  1;     // Cycles to perform
int MAINTAIN_DURATION =  10000; // Hold at temperature for 10 seconds
int PRINT_DELAY       =  1000;  // Print once every second (1000 ms)

boolean DEBUG =  false;

int current_cycle;
double PCR_time_start;

void setup() 
{
  Serial.println( "Setting up ... " );
  
  /* Set the data rate (baud) with which uno comms with comp */
  Serial.begin(9600);
  
  /* Heater and Fan are both outputs */
  pinMode( pin_heater_PWR, OUTPUT );   
  pinMode( pin_heater_DIR, OUTPUT );
  pinMode( pin_fan_PWR,    OUTPUT );
  pinMode( pin_fan_DIR,    OUTPUT ); 
  
  /* Set initial status of Heater and Fan */
  digitalWrite( pin_heater_DIR, HIGH ); // HIGH = 5V, LOW = 0V (Ground) 
  analogWrite ( pin_heater_PWR, 0    ); // Heater is on but not running
  digitalWrite( pin_fan_DIR,    LOW  ); // Fan polarity is LOW, not running
  analogWrite ( pin_fan_PWR,    0    ); // Fan is off
 
  current_cycle = 0;
  
  if ( DEBUG )
  { 
    
    Serial.println( "Testing fan ... " );
    runTest( pin_fan_PWR );
    fanOff();
    
    Serial.println( "Testing heater ... " );
    runTest( pin_heater_PWR );
    heaterOff();
    
    DEBUG = false;
    
  }
  
  Serial.println( "Setup complete.\n" );

  /* Start time for entire process */
  PCR_time_start = millis();

}

void loop() 
{
  
  if ( current_cycle < MAX_CYCLES )
  {
    // More cycles to go
    RampUp(); 
    Maintain( TEMP_CEILING );
    heaterOff();
    RampDown();
    delay( MAINTAIN_DURATION );    
    current_cycle++;
    
  }
  else if ( current_cycle == MAX_CYCLES )
  {
    // All cycles coompleted
    Serial.print( current_cycle++ );
    Serial.println(" cycles completed." );
    
    wrapUp();
    
    double total_running_time = ( millis() - PCR_time_start )/1000;
    Serial.print( "PCR process completed in " ); 
    Serial.print( total_running_time );
    Serial.println( " seconds." );
    
    sleep();
    
  }
  else { /*do nothing*/ }
  
}

void heaterOff()
{
  analogWrite( pin_heater_PWR, 0 );
}

void fanOff()
{
  analogWrite( pin_fan_PWR, 0 );
}

/* Return Current Temperature From Thermistor */
double Thermistor( int RawADC )
{
  //Temperature is currently in Kelvin
  double temp = log( 2000.0 * ( 1024.0 / RawADC-1 ) ); //         =log(10000.0/(1024.0/RawADC-1)) // for pull-up configuration
  temp = 1 / ( 0.00103514 + 0.000233825 * temp + ( 0.0000000792467 * temp * temp * temp ) );
  temp = temp - 273.15; // Kelvin to Celsius Conversion 
  
  return temp;

}

double checkPrint( double nextPrint, double ActualTemp )
{
    double clock = millis();
    if( clock >= nextPrint )
    {
      nextPrint = clock + PRINT_DELAY;
      int inSeconds = clock/1000;
      Serial.print( inSeconds );
      Serial.print( "s");
      Serial.print( "\t" );
      Serial.print( ActualTemp );
      Serial.println( " C" );
    }

    return nextPrint;
}

void runTest( int desired_pin )
{
  
  Serial.println( "Starting test ... " );
  analogWrite( desired_pin, 255 );
  
  double clock = millis();
  double stopTime = clock + MAINTAIN_DURATION;

  while ( clock < stopTime )
  {
    clock = millis();
  }
  
  analogWrite( desired_pin, 0 );
  Serial.println( "Ending test ... \n" );
}
 
void sleep()
{
  Serial.println( "Turning off heater and fan ... \n" ); 
  analogWrite( pin_heater_PWR, 0 );         // Heater is on but not running
  analogWrite( pin_fan_PWR, 0);
  
  Serial.println( "Putting device to sleep. Please restart the device." );
  
  set_sleep_mode( SLEEP_MODE_PWR_DOWN );
  cli(); 
  sleep_bod_disable();
  sei();
  sleep_cpu();
    
}

/* Turn Up the Heat */
void RampUp()
{
  Serial.println( "Ramping up ... " ); 
  double startTime = millis();  
  /* Get current temperature */
  double ActualTemp = Thermistor( analogRead( pin_thermistor ) );
  
  /* Increase heater output to 255 (255 = always on, 0 = always off) */
  analogWrite( pin_heater_PWR, 255 );
    
  int warning = 0;
  double timeToPrint = 0;
  while( ActualTemp < TEMP_CEILING ) 
  {
    /* Refresh current temperature */
    double newTemp = ( Thermistor( analogRead( 0 ) ) );
    
    /*
    if ( newTemp < ActualTemp )
    {
      if ( warning > WARNING_LIMIT )
      {
        Serial.println( "Error: Temperature not increasing on RampUp()." );
        Serial.println( "Check log for details." );
        sleep();
      }
      else { warning++; }
    }
    else { warning = 0; }
    */ 
    ActualTemp = newTemp;
  
    timeToPrint = checkPrint( timeToPrint, ActualTemp );
    
  }
  
  analogWrite( pin_heater_PWR, 0 );
  double totalTime = ( millis() - startTime )/1000;
  Serial.print( "RampUp finished in " );
  Serial.print( totalTime );
  Serial.println( " seconds.\n" );
    
}

/* Crank fan until cool enough */
void RampDown()
{
  double startTime = millis();
  double ActualTemp = Thermistor( analogRead( pin_thermistor ) );
  
  analogWrite( pin_fan_PWR, 255 );
  Serial.println( "Ramping down ... ");
  
  double timeToPrint = 0;
  //int warning = 0;
  while( ActualTemp > TEMP_FLOOR )
  {
    int newTemp = ( Thermistor( analogRead( 0 ) ) );
    
    /*
    if ( ActualTemp < newTemp )
    {
      if ( warning > WARNING_LIMIT )
      {
        Serial.println( "Error: Temperature not decreasing on RampDown()." );
        Serial.println( "Check log for details." );
        sleep();
      }
      else { warning++; }
    }
    else { warning = 0; }
    */
    ActualTemp = newTemp;
    
    timeToPrint = checkPrint( timeToPrint, ActualTemp );
    
  }
  
  analogWrite( pin_fan_PWR, 0 );
  double totalTime = ( millis() - startTime )/1000;
  Serial.print( "RampDown finished in " );
  Serial.print( totalTime );
  Serial.println( " seconds.\n" );

}

void wrapUp()
{
 
  Serial.println( "Wrapping up..." );
  double ActualTemp = Thermistor( analogRead( pin_thermistor ) );
  
  analogWrite( pin_fan_PWR, 255 );
  
  double timeToPrint = 0;
  //int warning = 0;
  while( ActualTemp > 50 )
  {
    int newTemp=( Thermistor( analogRead( 0 ) ) );
    /*
    if ( ActualTemp < newTemp )
    {
      if ( warning > WARNING_LIMIT )
      {
        Serial.println( "Error: Temperature not decreasing on WrapUp()." );
        Serial.println( "Check log for details." );
        sleep();
      }
      else { warning++; }
    }
    else { warning = 0; }
    */

    ActualTemp = newTemp;
    
    timeToPrint = checkPrint( timeToPrint, ActualTemp );
    
  }
  
  analogWrite( pin_fan_PWR, 0 );
  Serial.print( "Wrap finished. Fan is turning off. Current temperature: " );
  Serial.print( ActualTemp );
  Serial.println( "\n" );
}
  
void Maintain( int target )
{
  Serial.println( "Maintaining temperature ..." );  
  double time = millis();
  double stopTime = time + MAINTAIN_DURATION;
  
  double nextPrint = time + PRINT_DELAY;
  while ( time < stopTime )
  {
    time = millis();
    nextPrint = PID( Thermistor( analogRead( 0 ) ), target, true, time, nextPrint );
  }
  Serial.println( "\n" );
}

double PID( double Actual, double SetPt, boolean reset, double currentTime, double nextPrint ) 
{
  double kP=100, kI=0, kD=10;
  
  static double Previous_Error = 0, Integral = 0, time_one = 0;

  if( reset == true )
  {
    Previous_Error = 0;
    Integral = 0;
    time_one = 0;
  }

  double time_two = millis();
  double dt = ( time_two - time_one )/1000;
  
  double Error = SetPt-Actual;
  
  double P = kP*Error;
  double I = kI*(Integral + Error*dt);
  double D = kD*((Error - Previous_Error)/dt);
  
  double heat = (P+I+D);
  heat = saturation(heat);
  
  analogWrite( pin_heater_PWR,heat );
  Previous_Error=Error;
  Integral += Error*dt;
  time_one = time_two;
  

  nextPrint = checkPrint( nextPrint, Actual ); 
  
  return nextPrint;
  
}

double saturation(double heat)
{
  if( heat < 0 ) return 0;
  else if( heat > 255 ) return 255;
  else return heat;
}
